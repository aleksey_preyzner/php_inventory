<?php
error_reporting(E_ALL);
define('_INV',1);
define('DIR_BASE',str_replace("\\", "/",__DIR__));

//проверяем наличие файла конфигурации и подключаем его
if(file_exists(DIR_BASE.'/core/config.php')) {
    require_once DIR_BASE.'/core/config.php';
}
//проверяем переменную пути к ядру, если не объявлена - переходим в папку установки
if(!defined('DIR_CORE')) {
    header("Location: /install/index.php");
}
//класс реестра для вызова объектов
$registry = new Registry();

//создаем экземпляр класса обработчика запросов
$request = new Request();

//вносим обработчик запросов в реестр
$registry->set('request', $request);

//создаем объект для работы с БД, вносим его в реестр
$db = new MySQL(DB_HOST,DB_USER,DB_PASS,DB_NAME);
$registry->set('db',$db);

//обработчик ответов сервера/вывода контента, вносим в реестр
$responce = new Responce($registry);
$registry->set('responce',$responce);

//обработчик мета-данных документа, вносим в реестр
$document = new Document();
$registry->set('document',$document);

//объект для загрузки моделей и файлов, вносим в реестр
$load = new Loader($registry);
$registry->set('load',$load);

//объект обработки запросов, запускаем на обработку URL, выводим контент
$route = new Route($registry);
$route->Start();
$responce->addHeader('Content-Type: text/html; charset=utf-8');
$responce->output();

