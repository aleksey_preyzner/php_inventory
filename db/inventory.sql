-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.1.71-community-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных inventory
CREATE DATABASE IF NOT EXISTS `inventory` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `inventory`;


-- Дамп структуры для таблица inventory.category
CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(45) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `default_cat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `cat_sort` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`),
  UNIQUE KEY `cat_name_UNIQUE` (`cat_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица inventory.inventory
CREATE TABLE IF NOT EXISTS `inventory` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_name` varchar(45) NOT NULL,
  `inv_descr` varchar(255) DEFAULT NULL,
  `cat_id` int(11) NOT NULL,
  `inv_sort` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inv_id`),
  UNIQUE KEY `inv_name_UNIQUE` (`inv_name`),
  KEY `fk_inventory_category_idx` (`cat_id`),
  CONSTRAINT `fk_inventory_category` FOREIGN KEY (`cat_id`) REFERENCES `category` (`cat_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица inventory.stores
CREATE TABLE IF NOT EXISTS `stores` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(45) NOT NULL,
  `store_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `store_name_UNIQUE` (`store_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица inventory.stores_log
CREATE TABLE IF NOT EXISTS `stores_log` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_type` int(11) NOT NULL,
  `act_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inv_id` int(11) NOT NULL,
  `inv_quantity` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `act_comment` varchar(255) NOT NULL,
  PRIMARY KEY (`act_id`),
  KEY `fk_stores_log_stores1_idx` (`store_id`),
  KEY `act_type` (`act_type`),
  KEY `act_date` (`act_date`),
  KEY `inv_id` (`inv_id`),
  CONSTRAINT `stores_log_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `stores_log_inv_1` FOREIGN KEY (`inv_id`) REFERENCES `inventory` (`inv_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица inventory.store_inv
CREATE TABLE IF NOT EXISTS `store_inv` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `inv_quantity` int(11) NOT NULL,
  PRIMARY KEY (`pos_id`),
  KEY `fk_store_inv_stores1_idx` (`store_id`),
  KEY `fk_store_inv_inventory1_idx` (`inv_id`),
  CONSTRAINT `fk_store_inv_inventory1` FOREIGN KEY (`inv_id`) REFERENCES `inventory` (`inv_id`) ON DELETE CASCADE,
  CONSTRAINT `fk_store_inv_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
