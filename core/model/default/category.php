<?php
class Model_category extends Model {

    function addCategory($data) {
        if(isset($data['def_cat']) && ($data['def_cat'] == 1)) {
            if($this->checkCatDefaultIsSet()) {
               return FALSE;
            }
        }
        if(isset($data['parent_id'])) {
            $qstring = "INSERT INTO category (cat_name,parent_id) VALUES('".$data['cat_name']."',".$data['parent_id'].")";
        } else {
            $qstring = "INSERT INTO category (cat_name,default_cat) VALUES('".$data['cat_name']."',". $data['def_cat'].")";
        }
        $query = $this->db->query($qstring);
        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function DeleteCategory($id) {

        if($id != $this->GetDefCat()) {
            if($this->getParentCat($id) != NULL) {
                $cat_delete_query = "DELETE FROM `category` WHERE `cat_id`=".$id;
                $inv_move_query = "UPDATE `inventory` SET `cat_id`=". $this->getParentCat($id) ." WHERE `cat_id`=".$id;
            }
            else {
                //ищем все подкатегории, для которых удаляемая родительская
                $child_cat_query = $this->db->query("SELECT `cat_id` FROM `category` WHERE `parent_id`=".$id);
                $child_cat = $child_cat_query->rows;

                //добавляем родительскую категорию в массив удаляемых
                array_push($child_cat, array('cat_id' => $id));

                //добавляем все категории в массив
                foreach($child_cat as $remove_id) {
                    $cat_id[] = $remove_id['cat_id'];
                }
                //преобразуем массив категорий в строку для запроса
                $cat_id_query = "(". implode(",",$cat_id). ")";

                //запрос на удаление категорий и перенос инвентаря в категорию по умолчанию
                $inv_move_query = "UPDATE `inventory` SET `cat_id`=". $this->GetDefCat() ." WHERE `cat_id` IN". $cat_id_query;
                $cat_delete_query = "DELETE FROM `category` WHERE `cat_id`=".$id . " OR `parent_id`=".$id;
            }
            $this->db->query($inv_move_query);
            $this->db->query($cat_delete_query);

            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function getCategoryList() {
        $query = $this->db->query("SELECT * FROM `category` ORDER BY `cat_sort`,`cat_name` ASC");
        return $query->rows;
    }

    public function getParentCategories() {
        $query = $this->db->query("SELECT `cat_name`,`cat_id`,`cat_sort` FROM `category` WHERE `parent_id` IS NULL ORDER BY `cat_sort`,`cat_name` ASC");
        if($query->num_rows > 0) {
            return $query->rows;
        }
        else {
            return NULL;
        }
    }

    private function getParentCat($id) {
        $query = $this->db->query("SELECT `parent_id` FROM `category` WHERE `cat_id`=".$id);
        return current($query->row);
    }

    private function checkCatIsDefault($id) {
        $query = $this->db->query("SELECT `default_cat` FROM `category` WHERE `cat_id`=".$id);
        return (current($query->row) > 0) ? TRUE : FALSE;
    }

    private function checkCatDefaultIsSet() {
        $query = $this->db->query("SELECT `default_cat` FROM `category` WHERE `default_cat`=1 AND `parent_id` IS NULL");
        return ($query->num_rows > 0) ? TRUE : FALSE;
    }

    public function GetDefCat() {
        $query = $this->db->query("SELECT `cat_id` FROM `category` WHERE `default_cat`=1");
        return current($query->row);
    }

    public function SetDefCat($id,$def_id = self::GetDefCat) {
        $result = $this->db->query("UPDATE `category` SET `default_cat`=(CASE `cat_id` WHEN ".$def_id." THEN 0 WHEN ".$id." THEN 1 ELSE 0 END) WHERE `cat_id` IN(".$def_id.",".$id.")");
        return ($result) ? TRUE : FALSE;
    }
    public function renameCategory($data) {
        if($data['set_default'] && !$this->checkCatIsDefault($data['cat_id'])) {
            $this->SetDefCat($data['cat_id'],self::GetDefCat());
        }
        $result = $this->db->query("UPDATE `category` SET `cat_name`='".$data['cat_name']."',`cat_sort`=". $data['sort_ord'] ." WHERE cat_id=".$data['cat_id']);
        return ($result) ? TRUE : FALSE;
    }
    public function getSortId($id) {
        $query = $this->db->query("SELECT `cat_sort` FROM `category` WHERE `cat_id`=".$id);
        return $query->row['cat_sort'];
    }
    public function getStoreCatList($ids) {
        $query = $this->db->query("SELECT `cat_name` FROM `category` WHERE `cat_id` IN (". $ids .") ORDER BY `cat_sort`,`cat_name` ");
        return $query->rows;
    }
}