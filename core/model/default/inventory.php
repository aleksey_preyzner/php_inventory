<?php
class Model_inventory extends Model {
    public function getInventoryList($data) {
        $query = $this->db->query("SELECT * FROM inventory LEFT JOIN category USING(cat_id) ORDER BY inv_name ASC LIMIT ".$data['start'].",".$data['limit']);
        return $query->rows;
    }
    public function getTotalInventory() {
        $query = $this->db->query("SELECT COUNT(*) FROM inventory"); 
        return $query->row['COUNT(*)'];
    }
    public function getInventoryInfo($id) {
        $query = $this->db->query("SELECT * FROM inventory LEFT JOIN category USING(cat_id) WHERE inv_id=".$id);
        return $query->rows;
    }
    public function getInventoryLog($id) {
        $query = $this->db->query('SELECT stores_log.act_date,stores_log.act_type,stores_log.inv_id,stores_log.inv_quantity,stores_log.store_id,stores_log.act_comment,stores.store_name FROM stores_log LEFT JOIN stores USING(store_id) WHERE inv_id='.$id.' ORDER BY act_date DESC LIMIT 5');
        if($query->num_rows) {
        return $query->rows;  
        }
    }
    public function getInventoryStores($id) {
        $query = $this->db->query('SELECT store_inv.store_id,store_inv.inv_quantity,stores.store_name FROM store_inv LEFT JOIN stores USING(store_id) WHERE inv_id='. $id .' ORDER BY store_name ASC');
        if($query->num_rows) {
        return $query->rows;
        }
    }
    public function addNewInventory($data) {
        $query = $this->db->query("INSERT INTO inventory (inv_name,inv_descr,cat_id,inv_sort) VALUES ('". $data['inv_name']."','".$data['inv_descr']."','".$data['cat_id']."',".$data['inv_sort'].")");
        if($query) {
            return true;
        }
        else {
            return false;
        }
    }
    public function ChangeInventory($data) {
            if(is_array($data)) {
               if(($data['inv_descr']=='') || ($data['inv_descr']== NULL)) {
                   $querystr = "UPDATE inventory SET inv_name='".$data['inv_name']."',cat_id=".$data['cat_id'].",inv_sort=".$data['inv_sort']." WHERE inv_id=".$data['inv_id'];
               }
               else {
                   $querystr= "UPDATE inventory SET inv_name='".$data['inv_name']."',inv_descr='".$data['inv_descr']."',cat_id=".$data['cat_id'].",inv_sort=".$data['inv_sort']." WHERE inv_id=".$data['inv_id'];
               }
               $query=$this->db->query($querystr);
               if($query) {
                   return TRUE;
               }
               else {
                   return FALSE;
               }
            }
            else {
                return FALSE;
            }
    }
    public function DeleteInventory($inv_id) {
        $querystring = "DELETE FROM inventory WHERE inventory.inv_id IN (".$inv_id. ")";
        $query = $this->db->query($querystring);
        return($query ? TRUE : FALSE);
    }
}

