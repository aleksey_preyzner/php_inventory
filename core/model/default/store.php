<?php
class Model_store extends Model {

    public function getStoresList() {
        $query = $this->db->query("SELECT * FROM stores");
        return ($query->num_rows > 0) ? $query->rows : FALSE;
    }
    public function addStore($data) {
        $result = $this->db->query("INSERT INTO `stores` (`store_name`,`store_desc`) VALUES ('".$data["store_name"]."','".$data["store_descr"]."')");
        return ($result) ? TRUE : FALSE;
    }
    public function checkIdOnSt($sid,$id) {
        $result = $this->db->query("SELECT * FROM `store_inv` WHERE `inv_id`=".$id." AND `store_id`=".$sid);
        return ($result->num_rows > 0) ? TRUE : FALSE;
    }
    public function addInvStr($data = array()) {
       $result = $this->db->query("INSERT INTO `store_inv` (`store_id`,`inv_id`,`inv_quantity`) VALUES (".$data['store_id'].",".$data['inv_id'].",".$data['val'].")");
       return $result ? $result : FALSE;
    }
    public function delInvStr($data = array()) {
     $result = $this->db->query("DELETE FROM `store_inv` WHERE `store_id`=".$data["store_id"]." AND `inv_id`=".$data['inv_id']);
     return $result ? $result : FALSE;
    }
    public function deleteStore($id) {
        $result = $this->db->query("DELETE FROM `stores` WHERE `store_id`=".$id);
        return $result ? $result : FALSE;
    }
    public function updInvStr($data = array()) {
      $result = $this->db->query("UPDATE `store_inv` SET `inv_quantity` = `inv_quantity` + ".$data['val']." WHERE `store_id`=".$data['store_id']." AND `inv_id`=".$data['inv_id']);
       return $result ? $result : FALSE;
    }
    private function checkInvNum($i_id) {

    }
    public function getTotalInvOnCurStr($id) {
        $result = $this->db->query("SELECT SUM(inv_quantity) FROM `store_inv` WHERE `store_id`=".$id);
        return $result->row["SUM(inv_quantity)"];
    }
    public function getTotalPosOnCurStr($id) {
        $result = $this->db->query("SELECT COUNT(*) FROM `store_inv` WHERE `store_id`=".$id);
        return $result->row["COUNT(*)"];
    }
    public function getInvOnStr($id,$limit) {
        $result = $this->db->query("SELECT si.inv_id,si.inv_quantity,i.inv_name,i.cat_id,i.inv_sort,c.cat_name,c.cat_sort FROM store_inv AS si JOIN stores AS st JOIN inventory as i JOIN category as c ON(si.store_id = st.store_id AND si.inv_id = i.inv_id AND c.cat_id=i.cat_id AND si.store_id =". $id .") ORDER BY i.inv_sort,i.inv_name ASC LIMIT ".$limit['start'].",".$limit['limit']);
        return $result->rows;
    }
    public function getStoreName($id) {
        $result = $this->db->query("SELECT store_name FROM stores WHERE store_id=".$id);
        return $result->row['store_name'];
    }
    public function getCurInvOnCurStr($sid,$iid) {
        $result = $this->db->query("SELECT inv_quantity FROM store_inv WHERE inv_id=".$iid." AND store_id=".$sid);
        return $result->row['inv_quantity'];
    }
    public function getOtherStr($id) {
        $result = $this->db->query("SELECT store_id,store_name FROM stores WHERE store_id !=".$id);
        return $result->rows;
    }
}
