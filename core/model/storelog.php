<?php

class Model_storelog extends Model {

    public function getStoreLogs($data) {
        $query=$this->db->query("SELECT * FROM stores_log LEFT JOIN inventory USING(inv_id) LEFT JOIN stores USING(store_id) ORDER BY act_date DESC LIMIT ".$data['start'] .",".$data['limit']);
        return $query->rows;
    }
    public function getTotalLogs() {
      $query=$this->db->query("SELECT COUNT(*) FROM stores_log");
      return $query->row['COUNT(*)'];
    }
    public function setStoreLog($param = array()) {
        $result = $this->db->query("INSERT INTO `stores_log` (`act_type`,`inv_id`,`inv_quantity`,`store_id`,`act_comment`) VALUES(".$param["log"].",".$param["inv_id"].",".$param["val"].",".$param["store_id"].",'".$param["comment"]."')");
        return $result ? $result : FALSE ;
    }

}
