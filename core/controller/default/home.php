 <?php

class Controller_home extends Controller {
    public function index() {
        $this->template = "home.tpl";
        $this->document->SetTitle(SYSTEM_NAME." - Последние события");
        $this->document->addScript('inventory.js');
        $this->document->addScript('pagination.js');
        $this->children = array(
          "default/header",
          "default/footer",
            "default/menu",
            "default/content"
            );
        $this->responce->setOutput($this->render());
    }
}