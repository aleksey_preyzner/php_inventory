<?php
class Controller_header extends Controller {

    function index() {
        $this->document->addScript('jquery-ui-1.10.4.custom.min.js');
        $this->document->addScript('jquery-1.10.2.js');
        $this->document->addStyle("stylesheet.css");
        $this->document->addStyle("humanity/jquery-ui-1.10.4.custom.min.css");
        $this->data['doc_title'] = $this->document->GetTitle();
        $this->data['scripts'] = array_reverse($this->document->GetScripts());
        $this->data['styles'] = $this->document->getStyles();
        $this->template = "header.tpl";
        $this->render();
    }
}

