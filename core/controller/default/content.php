<?php
class Controller_content extends Controller {

    public function index() {
        $pagination = new Pagination();

                $this->data['title'] = "Последние события";
                $this->load->model('storelog');
                
                $pagination->total = $this->model_storelog->getTotalLogs();
                if(isset($this->request->get['page'])) {
                    $pagination->page = $this->request->get['page'];
                } else {
                    $pagination->page = 1;
                }
                $pagination->txt_request = "?";
                if(isset($this->request->get['limit'])) {
                           $pagination->limit = (int)$this->request->get['limit'];
                           $pagination->txt_request = "?limit=".$this->request->get['limit'];
                }
                $data = array (
                    'start' => ($pagination->page - 1)*($pagination->limit),
                    'limit' => $pagination->limit
                );
                
                $results = $this->model_storelog->getStoreLogs($data);
                
                foreach($results as $result) {
                    switch($result['act_type']) {
                        case 1:
                            $act_type = "<strong class='add'>Пришло на склад</strong>";
                            break;
                        case 2:
                            $act_type = "<strong class='move'>Перемещено со склада</strong>";
                            break;
                        case 3:
                            $act_type ="<strong class='delete'>Списано со склада</strong>";
                        }
                    
                    $this->data['stores_logs'][] = array(
                        'act_type'          => $act_type,
                        'act_date'          => date('d.m.Y H:i',strtotime($result['act_date'])),
                        'inv_name'          => $result['inv_name'],
                        'inv_id'            => $result['inv_id'],
                        'quantity'          => $result['inv_quantity'],
                        'store_name'        => $result['store_name'],
                        'act_comment'       => $result['act_comment']
                    );
                }
                $this->data['pagination'] = $pagination->render();
                
                $this->template = "main.tpl";
                $this->render();
                }

    }