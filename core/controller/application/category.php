<?php
class Controller_category extends Controller {
    private $id;
    
    public function index() {
        $this->load->model('default/category');
        $this->document->SetTitle(SYSTEM_NAME." - Управление категориями");
        $this->document->addScript("category.js");
        $results = $this->model_default_category->getCategoryList();
        if(count($results)>0) {
            foreach($results as $result) {
                if($result['parent_id']!=NULL) {
                $this->data['children_cats'][] = array(
                    'cat_id'    => $result['cat_id'],
                    'cat_name'  => $result['cat_name'],
                    'parent_id' => $result['parent_id']
                    );
                }
                else {
                    $this->data['parent_cats'][] = array(
                        'cat_id'    => $result['cat_id'],
                        'cat_name'  => $result['cat_name'],
                        'def_cat'   => $result['default_cat']
                    ); 
                }
                
             
            }            
        }
        $this->data['title'] = "Список категорий";
        $this->template = "category/category_list.tpl";
        $this->children = array(
          "default/header",
          "default/footer",
          "default/menu",
            );
        $this->responce->setOutput($this->render());
    }
    public function addCategory() {
        $data = array();
        if(isset($this->request->post['cat_name'])) {
            if(isset($this->request->post['main_cat']) && ($this->request->post['main_cat'] == 'on')) {
                if(isset($this->request->post['def_cat']) && ($this->request->post['def_cat'] == 'on')) {
                    $data = array(
                        'def_cat'   => 1,
                        'cat_name'  => $this->request->post['cat_name'],
                        'sort_ord'  => intval($this->request->post['sort'])
                    );
                }
                else {
                    $data = array(
                        'def_cat'   => 0,
                        'cat_name'  => $this->request->post['cat_name'],
                        'sort_ord'  => intval($this->request->post['sort'])
                    );

                }
            }
            else {
               $data = array (
                   'cat_name'   => $this->request->post['cat_name'],
                   'parent_id'  => $this->request->post['main-cat-id'],
                   'sort_ord'   => intval($this->request->post['sort'])
               ); 
            }
            $this->load->model('default/category');
            $result = $this->model_default_category->addCategory($data);
            
            if($result) {
                echo "<span class='success'>Категория добавлена!</span>";
            } else {
                echo "<span class='error'>Не удалось добавить категорию!</span>";
            }
        }
        else {
            echo "<span class='error'>Не удалось добавить категорию!</span>";
        }
                
    }
    public function deleteCategory() {
        if(isset($this->request->post['cat_id'])) {
            $this->id = intval($this->request->post['cat_id']);
        }
        else {
           echo "<span class='error'>Не указан идентификатор категории!</span>";
        }

        $this->load->model('default/category');
        $request = $this->model_default_category->DeleteCategory($this->id);
        if($request) {
        echo "<span class='success'>Категория удалена!</span>";
        }
        else {
        echo "<span class='error'>ОШИБКА! Не удалось удалить категорию!</span>";
        }
    }
    public function renameCategory() {
        if(isset($this->request->post['rcat_id']) && ($this->request->post['rcat_id'] != NULL)) {
            $id = (int)($this->request->post['rcat_id']);
            if(isset($this->request->post['rcat_name']) && ($this->request->post['rcat_name'] != '')) {
                $name = htmlentities($this->request->post['rcat_name']);
                $sort = $this->request->post["sort"];
                if(isset($this->request->post['rcat-def']) && ($this->request->post['rcat-def'] == 'on')) {
                    $def = TRUE;
                }
                else {
                    $def = FALSE;
                }
                $data = array(
                  'cat_id'      => $id,
                  'cat_name'    => $name,
                  'set_default' => $def,
                  'sort_ord'    => $sort
                );
                $this->load->model('default/category');
                $result = $this->model_default_category->renameCategory($data);
                if($result) {
                    echo "<span class='success'>Категория отредактирована!</span>";
                }
                else {
                    echo "<span class='error'>Ошибка! Категория не отредактирована!</span>";
                }
            }
            else {
                echo "<span class='error'>Ошибка! Не указанно имя категории!</span>";
            }
        }
        else {
            echo "<span class='error'>Ошибка! Неверно указан или не существует id категории!</span>";
        }
        
    }
    public function listCategory() {
        $this->load->model('default/category');
        $result = $this->model_default_category->getCategoryList();
        if(isset($this->request->post["ajax"]) && ($this->request->post["ajax"] == 1)) {
            header('Content-type: application/json');
            echo json_encode($result);
        } else {
            return $result;
        }
    }
    public function listParentCategory() {
        $this->load->model('default/category');
        $result = $this->model_default_category->getParentCategories();
        header('Content-type: application/json');
        echo json_encode($result);
    }
    public function categoryform() {
        $this->template="category/category_add.tpl";
        echo $this->render();
    }
    public function renameForm() {
        $this->template="category/category_rename.tpl";
        echo $this->render();
    }
    public function getSortId() {
        $id = $this->request->post["id"];
        $this->load->model("default/category");
        $result = $this->model_default_category->getSortId($id);
        echo $result;
    }
}