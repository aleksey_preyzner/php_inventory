<?php

class Controller_reports extends Controller {
    private $s_id = 1;

    public function index() {
        $this->document->SetTitle(SYSTEM_NAME." - Отчеты");
        $this->document->addScript("reports.js");


        $this->template = "reports/main.tpl";
        $this->children = array(
          "default/header",
          "default/footer",
          "default/menu",
        );
        $this->responce->setOutput($this->render());
    }
    public function ReportStore() {
        $this->s_id = $this->request->post['store_id'];
        $this->load->model("report/rstore");
    }

}
?>