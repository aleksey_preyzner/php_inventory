<?php
class Controller_inventory extends Controller {

    public function index() {
      $this->document->SetTitle(SYSTEM_NAME." - Управление инвентарем");
      $this->document->addScript('inventory.js');
      $this->document->addScript('pagination.js');
      $pagination = new Pagination();
      $this->load->model('default/inventory');
                        
                        
                        $pagination->total = $this->model_default_inventory->getTotalInventory();
                        if(isset($this->request->get['page'])) {
                            $pagination->page = $this->request->get['page'];
                        } else {
                            $pagination->page = 1;
                        }
                       $pagination->txt_request = "?route=".$this->request->get['route'];
                       if(isset($this->request->get['limit'])) {
                           $pagination->limit = (int)$this->request->get['limit'];
                           $pagination->txt_request .= "&limit=".$pagination->limit;
                       }
                        $data = array (
                            'start' => ($pagination->page - 1)*($pagination->limit),
                            'limit' => $pagination->limit
                        );
                        
                        $results = $this->model_default_inventory->getInventoryList($data);
                        
                        foreach ($results as $result) {
                     
                            $this->data['inventory_lists'][] = array(
                                'inv_id'    => $result['inv_id'],
                                'inv_name'  => $result['inv_name'],
                                'inv_descr' => $result['inv_descr'],
                                'cat_name'  => $result['cat_name']
                            );
                        }
                        $this->data['title'] = "Список инвентаря";
                        $this->data['pagination'] = $pagination->render();
                        
                        $this->template="inventory/inventory_list.tpl";
                                $this->children = array(
          "default/header",
          "default/footer",
          "default/menu"
            );
                       $this->responce->setOutput($this->render());
    }
    private function getinvlog($id) {
        $this->load->model('default/inventory');
        $results = $this->model_default_inventory->getInventoryLog($id);
        if($results) {
        foreach($results as $result) {
            switch($result['act_type']) {
                        case 1:
                            $act_type = "<strong class='add'>Пришло на склад</strong>";
                            break;
                        case 2:
                            $act_type = "<strong class='move'>Перемещено со склада</strong>";
                            break;
                        case 3:
                            $act_type ="<strong class='delete'>Списано со склада</strong>";
                        }
                        
            $data[] = array (
                'act_date'      => date('d.m.Y H:i',strtotime($result['act_date'])),
                'act_type'      => $act_type,
                'store_id'      => $result['store_id'],
                'store_name'    => $result['store_name'],
                'quantity'      => $result['inv_quantity'],
                'comment'       => $result['act_comment']
            );
           }
           return $data;
        }   
        else {
            return NULL;
        }
    }
    private function getinvstore($id) {
        $this->load->model('default/inventory');
        $results = $this->model_default_inventory->getInventoryStores($id);
        if($results) {
        foreach($results as $result) {
            $data[] = array (
                'store_name'    => $result['store_name'],
                'store_id'      => $result['store_id'],
                'quantity'      => $result['inv_quantity']
            );
        }
        return $data;
        } else {
            return NULL;
        }
        
    }
    public function info() {
        if(isset($this->request->get['id'])) {
            $id=$this->request->get['id'];
        } else {
            echo "<div class='box-title'>&nbsp;</div><div class='box-content'>Инвентарь не найден!<p onClick=\"$('#pop_up').fadeOut();\">Закрыть</p></div>";
        }
        $this->load->model('default/inventory');
        $results = $this->model_default_inventory->getInventoryInfo($id);
        foreach($results as $result) {
            $this->data['inventorys'][] = array(
                'inv_name'      => $result['inv_name'],
                'inv_id'        => $result['inv_id'],
                'inv_descr'     => $result['inv_descr'],
                'inv_sort'      => $result['inv_sort'],
                'cat_name'      => $result['cat_name'],
                'cat_id'        => $result['cat_id']
            );
        }
        $this->data['invlogs'] = $this->getinvlog($id);
        $this->data['invstores'] = $this->getinvstore($id);
        $this->template="inventory/inventory_info.tpl";
        echo $this->render();
    }

    public function inventoryform() {
        $this->load->model('default/category');
        $results=$this->model_default_category->getCategoryList();
        foreach($results as $result) {
            $this->data['cat_lists'][] = array(
                'cat_id'    => $result['cat_id'],
                'cat_name'  => $result['cat_name']
            );
        }
        $this->data['title'] = "Добавление инвентаря";
        $this->template="inventory/inventory_add.tpl";
        echo $this->render();   
    }
    public function add_inventory() {
        $data['inv_name'] = strval($this->request->post['inv_name']);
        $data['cat_id'] = intval($this->request->post['cat_id']);
        $data['inv_descr'] = strval($this->request->post['inv_descr']);
        if(isset($this->request->post['sort']) && ($this->request->post['sort'] != "")) {
            $data['inv_sort']   = intval($this->request->post['sort']);
        } else {
            $data['inv_sort']   = 0;
        }
        $this->load->model('default/inventory');
        $result = $this->model_default_inventory->addNewInventory($data);
        if($result == true) {
            echo "<span class='success'>Инвентарь добавлен!</span>";
        }
        else {
            echo "<span class='error'>Не удалось добавить инвентарь!</span>";
        }
    }
    public function change_inventory() {
            if(isset($this->request->request['info_inv_id'])) {
                $data = array(
                    'inv_id'    => intval($this->request->request['info_inv_id']),
                    'inv_name'  => $this->request->request['info_inv_name'],
                    'cat_id'    => $this->request->request['info_cat_id'],
                    'inv_descr' => $this->request->request['info_inv_descr'],
                    'inv_sort'  => intval($this->request->request['sort'])
                    );
                $this->load->model('default/inventory');
                $result = $this->model_default_inventory->ChangeInventory($data);
                if($result) {
                        echo "<span class='success'>Инaформация об инвентаре обновлена!</span>";
                    }
                    else {
                        echo "<span class='error'>Ошибка обновления информации!</span>";
                    }
                }
           else {
                    echo "Это не Ajax, идите лесом!";
                }
       }
   public function delete_inventory() {
       if(isset($this->request->post["data"])) {
           $data = str_replace("_", ",",$this->request->post["data"]);
           $this->load->model('default/inventory');
           $result = $this->model_default_inventory->DeleteInventory($data);
           if($result) {
               echo "<span class='success'>Инвентарь успешно удален!</span>";
           }
           else {
               echo "<span class='error'>Ошибка запроса! Обратитесь к администратору!</span>";
           }
       }
       else {
        echo "<span class='error'>Ошибка запроса к базе данных! Обратитесь к администратору.</span>";
       }
   }
}

