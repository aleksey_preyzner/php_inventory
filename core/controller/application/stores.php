<?php
class Controller_stores extends Controller {

    public function index() {
        $this->document->SetTitle(SYSTEM_NAME." - Управление складами");
        $this->document->addScript("stores.js");
        $this->load->model('default/store');
        $results = $this->model_default_store->getStoresList();
      if($results) {  
        foreach($results as $result) {
            $this->data['stores'][] = array(
              'store_id'    => $result['store_id'],
              'store_name'  => $result['store_name'],
              'store_desc'  => $result['store_desc'],
              'store_total' => $this->getTotalInvOnStore($result['store_id'])
            );
        }
      }
        $this->data['title'] = "Список складов";
            $this->template = "store/store_list.tpl";
            $this->children = array(
          "default/header",
          "default/footer",
          "default/menu",
            );
       $this->responce->setOutput($this->render());

    }

    public function view_store_inv() {
       if(isset($this->request->get['store_id'])) {
           $s_id = $this->request->get['store_id'];
           
           $this->document->addScript("stores.js");
           $this->document->addScript("inventory.js");
           $this->load->model('default/store');

           $pagination = new Pagination();
           $pagination->total = $this->model_default_store->getTotalPosOnCurStr($s_id);
           if(isset($this->request->get['page'])) {
                            $pagination->page = $this->request->get['page'];
                        } else {
                            $pagination->page = 1;
                        }
                       $pagination->txt_request = "?route=".$this->request->get['route']."&store_id=".$this->request->get['store_id']."&";
                        $data = array (
                            'start' => ($pagination->page - 1)*($pagination->limit),
                            'limit' => $pagination->limit
                        );

           $results = $this->model_default_store->getInvOnStr($s_id,$data);

           if($results) {
           foreach($results as $result) {
               $catid_list[]     = array (                     //список категорий
                   'cat_id'         => $result['cat_id'],
                   'cat_name'       => $result['cat_name'],
                   'cat_sort'       => $result['cat_sort']
                       
               );
               $inventory_list[] = array(                     //список инвентаря на складе
                   'inv_id'         => $result['inv_id'],
                   'inv_name'       => $result['inv_name'],
                   'cat_id'         => $result['cat_id'],
                   'inv_quantity'   => $result['inv_quantity'],
                   'inv_sort'       => $result['inv_sort']
               );
           }
           $catid_list = array_unique($catid_list,SORT_REGULAR);// убираем повторяюзиеся категории в массиве

           // пузырьки для категорий
           function cmp_cat($a,$b) {
                if($a['cat_sort'] == $b['cat_sort']) {
                    return 0;
                    }
                return ($a['cat_sort'] < $b['cat_sort']) ? -1 : 1;
                }
            // пузырьки для инвентаря
            function cmp_inv($a,$b) {
                if($a['inv_sort'] == $b['inv_sort']) {
                    return 0;
                    }
                return ($a['inv_sort'] < $b['inv_sort']) ? -1 : 1;
                }

            // сортируем оба массива
            usort($catid_list,'cmp_cat');
            usort($inventory_list,'cmp_inv');

           foreach($catid_list as &$cat) {
               foreach($inventory_list as &$inv) {
                   if($inv['cat_id'] == $cat['cat_id']) {
                   $cat['inv_list'][] = $inv;
                   }
               }

           }
           unset($cat);
           unset($inv);


           $this->data["inv_list"]      = $catid_list;
           }
           $this->data["store_id"]      = $s_id;
           $this->data["store_name"]    = $this->model_default_store->getStoreName($s_id);
           $this->data["title"]         = "Инвентарь на складе " . $this->data['store_name'];
           $this->data['pagination']    = $pagination->render();
           $this->data['total']         = $this->getTotalInvOnStore($s_id);

           $this->document->SetTitle(SYSTEM_NAME." - Инвентарь на складе " . $this->data['store_name']);

           $this->template="store/inv_on_store_list.tpl";
           $this->children = array(
               "default/header",
               "default/footer",
               "default/menu"
           );
           $this->responce->setOutput($this->render());
       }
       else {
           $this->responce->redirect("/");
       }
    }

    public function storeList() {
        if(isset($this->request->get['store_id']) && (is_int($this->request->get['store_id'])) )
        {
            $query_type = 1;
        }
        else {
            $query_type = 0;
        }
        $query = $this->load->model('default/stores');
        $query->getStoresList($query_type);
    }
    public function addForm() {
        $this->template="store/store_add.tpl";
        echo $this->render();
    }
    public function addStore() {
        if(isset($this->request->post['store_name']) && ($this->request->post['store_name'] != '')) {
            $data = array(
                'store_name'    => htmlentities($this->request->post['store_name'],ENT_QUOTES, "UTF-8"),
                'store_descr'   => htmlentities($this->request->post['store_descr'],ENT_QUOTES, "UTF-8")
            );
            $this->load->model('default/store');

            $result = $this->model_default_store->addStore($data);
            if($result) {
                echo "<span class='success'>Склад успешно добавлен!</span>";
            } else {
                echo "<span class='error'>Склад не добавлен!</span>";
            }
        }
        else {
            echo "<span class='error'>Ошибка! Имя склада не может быть пустым!</span>";
        }

    }
    public function deleteStore($id) {
        if(isset($this->request->post['s_id']) && ($this->request->post['s_id'] != '') ) {
            $id = $this->request->post['s_id'];
            $this->load->model('default/store');
            $result = $this->model_default_store->deleteStore($id);
                    if($result) {
                        echo "<span class='success'>Склада удален!</span>";
                    }
                    else {
                        echo "<span class='error'>Ошибка удаления склада!</span>";
                    }
        }
        else {
            echo "<span class='error'>Не выбран склад для удаления!</span>";
        }
    }
    public function AddInvToStoreForm() {
        $this->template="store/store_add_inv.tpl";
        $this->load->model("default/inventory");
        $inv_list = $this->model_default_inventory->getInventoryList(array("start" => 0, "limit" => $this->model_default_inventory->getTotalInventory()));
        foreach ($inv_list as $inv) {
            $this->data['inv_list'][] = array(
                'inv_id'    => $inv['inv_id'],
                'inv_name'  => $inv['inv_name']
            );
        }
        echo $this->render();
    }
    public function addInvToStore() {
        if(isset($this->request->post['ainv_id']) && (isset($this->request->post['astore_id'])) && ($this->request->post['ainv_id'] != NULL)  &&($this->request->post['astore_id'] != NULL) && is_numeric($this->request->post['ainv_id']) && is_numeric($this->request->post['astore_id'])) {
            $inv_id = $this->request->post['ainv_id'];
            $store_id = $this->request->post['astore_id'];
            if(isset($this->request->post['aval']) && (isset($this->request->post['astore_com'])) && ($this->request->post['aval'] != NULL) && ($this->request->post['astore_com'] != NULL) && is_numeric($this->request->post['aval'])) {
                $val = $this->request->post['aval'];
                $comment = htmlspecialchars($this->request->post['astore_com']);
                $data = compact("inv_id", "store_id", "val", "comment");
                if($this->checkInvIsOnStore($store_id,$inv_id)) {
                    $this->UpdateInvOnStore("update",$data);
                }
                else {
                    $this->UpdateInvOnStore("add",$data);
                }
                echo "<span class='success'>Инвентарь добавлен в количестве ".$val." шт.</span>";
            } else {
                echo "<span class='error'>Неверно указано количество или отсутствует коментарий!</span>";
            }
        } else {
            echo "<span class='error'>Не указан id инвентаря или склада!</span>";
        }
    }

    public function RemoveInvFromStore() {
        if(isset($this->request->post["action"])) {
            $store_id       = $this->request->post["s_id"];
            $inv_id       = $this->request->post["inv_id"];
            $val      = $this->request->post["val"];
            $comment    = htmlspecialchars($this->request->post["comnt"]);
            $act        = $this->request->post["action"];

            $this->load->model("default/store");
            $inv_max = $this->model_default_store->getCurInvOnCurStr($store_id,$inv_id);

            $data       = compact("inv_id", "store_id", "val", "comment");

                switch($act) {
                    case 1:
                        if($val == $inv_max) {
                        $this->UpdateInvOnStore("delete", $data);
                        } elseif($val < $inv_max) {
                        $this->UpdateInvOnStore("remove",$data);
                        }
                        echo "<span class='success'>Инвентарь списан со склада!</span>";
                        break;
                    case 2:
                        if($val == $inv_max) {
                            $this->UpdateInvOnStore("delete", $data);
                        } elseif($val < $inv_max) {
                            $this->UpdateInvOnStore("move",$data);
                        }
                        $data['store_id'] = $this->request->post["store"];
                            if($this->checkInvIsOnStore($data['store_id'],$data['inv_id'])) {
                            $this->UpdateInvOnStore("update", $data);
                            } else {
                             $this->UpdateInvOnStore("add", $data);
                            }
                        echo "<span class='success'>Инвентарь успешно перенесен!</span>";
                        break;
                }
        }
        else {
            echo "<span class='error'>Ошибка! Не указанно действие.</span>";
        }
    }

    public function RemInvFromStoreForm() {
        $this->template="store/store_rem_inv.tpl";
        echo $this->render();
    }

    private function checkInvIsOnStore($s_id,$i_id) {
        $this->load->model('default/store');
        $result = $this->model_default_store->checkIdOnSt($s_id,$i_id);
        return ($result ? $result : FALSE);
    }
    private function UpdateInvOnStore($op, $data = array()) {
        $this->load->model('default/store');
        switch ($op) {
            case "add":
                $result = $this->model_default_store->addInvStr($data);
                $data["log"] = 1;
                $this->addLogEvent($data);
                break;
            case "update":
                $result = $this->model_default_store->updInvStr($data);
                $data["log"] = 1;
                $this->addLogEvent($data);
                break;
            case "move":
                $data["val"] = -$data["val"];
                $result = $this->model_default_store->updInvStr($data);
                $data["log"] = 2;
                $data["val"] = -$data["val"];
                $this->addLogEvent($data);
                break;
            case "remove":
                $data["val"] = -$data["val"];
                $result = $this->model_default_store->updInvStr($data);
                $data["log"] = 3;
                $data["val"] = -$data["val"];
                $this->addLogEvent($data);
                break;
            case "delete":
                $result = $this->model_default_store->delInvStr($data);
                $data["log"] = 3;
                $this->addLogEvent($data);
                break;
        }
        return $result ? $result : FALSE;
     }
    private function addLogEvent($param = array()) {
        $this->load->model("storelog");
        $result = $this->model_storelog->setStoreLog($param);
        return ($result) ? $result : FALSE;
    }
    public function getTotalInvOnStore($id) {
        $this->load->model("default/store");
        $result = $this->model_default_store->getTotalInvOnCurStr($id);
        return $result;
    }
    public function getTotalPosOnStore($id) {
        $this->load->model("default/store");
        $result = $this->model_default_store->getTotalPosOnCurStr($id);
        return $result;
    }
    public function getCurInvOnStore() {
        $s_id = $this->request->post['sid'];
        $i_id = $this->request->post['iid'];
        $this->load->model("default/store");
        $result = $this->model_default_store->getCurInvOnCurStr($s_id,$i_id);
        echo $result ? $result : 0;
    }
    public function getOtherStores() {
        $sid = $this->request->post['sid'];
        $this->load->model("default/store");
        $result = $this->model_default_store->getOtherStr($sid);
        header('Content-Type: application/json');
        echo json_encode($result);
    }
}

