<?php
defined('_INV') or die('Restricted access');

define('SYSTEM_NAME','Inventory');
define('SYSTEM_VERSION','0.9 RC1');
define('SYSTEM_HOST',$_SERVER['HTTP_HOST']);
/*define config path*/

define('DIR_CORE',str_replace("\\","/",__DIR__)."/");
define('DIR_THEME',DIR_CORE."view/template/");
define('DIR_CSS',SYSTEM_HOST."/core/view/template/css/");
define('DIR_IMG',SYSTEM_HOST."/core/view/template/image/");
define('DIR_JS',SYSTEM_HOST."/core/view/template/js/");
define('DIR_LIB',DIR_CORE."lib/");

/*Define BD data*/
define('DB_HOST','localhost');
define('DB_NAME','inventory');
define('DB_USER','root');
define('DB_PASS','');

/* подключаем библиотеки */

require_once DIR_LIB.'mysql.php';
require_once DIR_LIB.'request.php';
require_once DIR_LIB.'responce.php';
require_once DIR_LIB.'registry.php';
require_once DIR_LIB.'action.php';
require_once DIR_LIB.'document.php';
require_once DIR_LIB.'pagination.php';

require_once 'controller.php';
require_once 'model.php';
require_once 'route.php';
require_once 'loader.php';