<?php
echo $header;
?>
<div id="left-column">
 <?php
 if(isset($menu)) {
 echo $menu;
 }
 ?>
</div>
<div id="center-column">
<div class="block-title">
    <?php echo $title ?>
</div>
<div class="block-content">
    <div id="options">
        <span class="button" onClick="javascript:add_inv_dialog();" >Добавить инвентарь</span>
        &nbsp; Показывать:<select id="limit" onChange="setLimit();">
            <option value="15">15</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="75">75</option>
        </select>&nbsp;<select id="act-type"><option>Удалить</option></select>&nbsp;<span class="button" onClick="inv_del();">Применить</span><span class='info'></span>
    </div>
    <?php
if(isset($inventory_lists)) { ?>
<center>
    <?php echo $pagination; ?>
    <table class="log" cellspacing="0" cellpadding="0">
 <tr class="log-title">
        <td colspan="2">Название
        </td>
        <td>Категория
        </td>
        <td width="350px">Описание
        </td>
<?php
foreach ($inventory_lists as $list) {
?>
<tr>
    <td style="width:20px;"><input type="checkbox" value="<?php echo $list['inv_id']; ?>">
    </td>
    <td><a href="javascript:void(0);" onClick="javascript:info_inv_dialog(<?php echo $list['inv_id']; ?>);"><?php echo $list['inv_name']; ?></a>
    </td>
    <td><?php echo $list['cat_name']; ?>
    </td>
    <td><?php echo $list['inv_descr']; ?>
    </td>
</tr>
<?php } ?>
</table><?php echo $pagination; ?></center>
<script>
     $("tr:even").addClass("even");
</script>
<?php } else {
echo "Нет инвентаря в списке!";
}
?>
</div>
</div>
<?php
echo $footer;
?>