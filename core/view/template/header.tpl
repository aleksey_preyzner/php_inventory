<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Inventory System" />
<meta name="author" content="Alexey Preyzner" />
<title><?php if(isset($doc_title)) { echo $doc_title; }?></title>
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="//<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php
if(isset($scripts)) {
foreach($scripts as $script) { ?>
<script src='http://<?php echo DIR_JS.$script; ?>' type='text/javascript'></script>
<?php   }
}
?>
<script>
$(function() {
$( document ).tooltip();
});
</script>
</head>
<body>
<div id="conteiner">
    <div id="dialog"></div>
    <div id="header">
        <div id="welcome">
     </div>
 <div id="title"><h1><a class="header" href="http://<?php echo $_SERVER['HTTP_HOST']?>" target="_self"><?php echo SYSTEM_NAME; ?></a></h1>
        </div>
        </div>
<div id="content">