<?php
echo $header;
?>
<div id="left-column">
 <?php
 if(isset($menu)) {
 echo $menu;
 }
 ?>
</div>
<div id="center-column">
<div class="block-title">
    <?php echo $title ?>
</div>
<div class="block-content">
    <div id="options"><span class="button" onClick="javascript:add_cat_dialog();">Добавить категорию</span><span class='info'></span></div>
    <?php if(isset($parent_cats)) {
    echo "<ul class='parent-list'>";
    foreach($parent_cats as $parent_cat) {
    echo "<li class='parentcat";
    if($parent_cat['def_cat'] == 1) {
    echo " def-cat";
    }
    echo "'><span class='pointer boldtxt' onClick='$(this).parent(\".parentcat\").children(\"ul.children-list\").slideToggle(300);'>".$parent_cat['cat_name']."</span><a class='edit-link pointer' title='Редактировать' onclick='javascript:rename_form($(this),".$parent_cat['cat_id'].");'></a><a class='del-link pointer' onClick='javascropt:delcat(".$parent_cat['cat_id'].");' title='Удалить категорию'></a>";
    echo "<ul class='children-list'>";
        if(isset($children_cats)) {
            foreach($children_cats as $children_cat) {
                if($parent_cat['cat_id'] == $children_cat['parent_id']) {
                echo "<li class='childrencat'><span>".$children_cat['cat_name']."</span><a class='edit-link pointer' title='Редактировать' onclick='javascript:rename_form($(this),".$children_cat['cat_id'].");'></a><a class='del-link pointer' onClick='javascript:delcat(".$children_cat['cat_id'].");' title='Удалить категорию'></a></li>";
            }
        }
    }
      echo "</ul></li>";
    }
   echo "</ul>";
} else {
echo "Нет категорий для отображения";
}
?>
<ul>
<li class="def-cat"> - категория по умолчанию.</li>
</ul>
</div>
</div>
<?php
echo $footer;
?>