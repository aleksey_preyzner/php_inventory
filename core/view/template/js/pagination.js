$(document).ready(function() {
    if(window.location.search.indexOf("?limit=") > -1 || window.location.search.indexOf("&limit=") > -1) {
        var str = window.location.search.lastIndexOf("limit=") + 6;
        var def_limit = window.location.search.slice(str,str + 3).replace(/&/g,"");
        $("#limit option[value='" + def_limit + "']").prop("selected",true);

    }
});
function setLimit() {
            if(window.location.toString().indexOf("?route") < 0) {
            var param = "?limit=";
            repLimit(param);
        } else {
            var param = "&limit=";
            repLimit(param);
        }
}
function repLimit(param) {        
    if(window.location.toString().indexOf(param) < 0) {
    window.location.href = window.location.href + param + $("#limit").val();
    } else {
        var limit = window.location.toString().lastIndexOf(param) + 7;
        window.location.href = window.location.toString().slice(0,limit) + $("#limit").val() + window.location.toString().slice(limit + 2);
    }
}

