/* Add Category Section START*/
function check_parent_cat() {
    if(!$('#main_cat').is(':checked')) {
        add_parent_cat('#main-cat-id');
        $('#def_cat').prop('disabled',true);
    } else {
        $('#def_cat').removeAttr('disabled');
        $('#main-cat-id').empty();
        $('#main-cat-id').append("<option>Выберите категорию</option>");
        $('#main-cat-id').prop('disabled', true); 
    }
}
function add_parent_cat(pos) {
    $.ajax({
            url:"?route=application/category/listParentCategory",
            dataType:"json",
            cache:false,
            beforeSend:function() {
             $(pos).removeAttr('disabled');
             $(pos).css("display","none");
             $(pos).parent().append("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
            },
            success:function(data) {
                var parent_cat = data;
                $(pos).parent().children('img').remove();
                $(pos).css("display","inline");
                $(pos).empty();
                for(var i = 0; i < parent_cat.length; i++) {
                    $(pos).append("<option value='"+ parent_cat[i].cat_id + "'>" + parent_cat[i].cat_name + "</option>");  
            }
            }
            
        });
}
function add_cat() {
    var catform = $('#addcat').serialize();
    $.ajax({
        url:"?route=application/category/addCategory",
        type:"POST",
        data:catform,
        cache:false,
        error:function(xhr, str) {
                $('.info').html('<span class="error">Ошибка:'+ xhr.responseCode + '</span>');
            },
        success: function(data) {$('.info').html(data);setTimeout(function(){location.reload();},3000);},
        beforeSend: function() {$('.info').append("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");;}
    });
}
function add_cat_dialog() {
    var dialog = open_dialog();
    dialog.dialog({
        title:"Добавить категорию",
        buttons: {
         "Добавить":function() {add_cat();dialog.dialog("close");},
         "Отмена":function() {dialog.dialog("close");}
        }   
    });
    dialog.load('?route=application/category/categoryform',function() {dialog.dialog("open");});
}
/*Add Category Section END*/

/*Delete Category Section START*/
function delcat(cat_id,pos) {
    if(typeof(pos) === "undefined") {
        pos = ".info";
    }
    if(!typeof(cat_id) === "number") {
        alert("ID категории не является числом!");
        return false;
    } else {
    var accept = confirm("Вы действительно хотите удалить категорию?");
    if(accept) {
    $.ajax({
        url:"?route=application/category/deleteCategory",
        type:"POST",
        data:"cat_id="+ cat_id,
        cache:false,
        beforeSend:function() {
            $(pos).html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
        }
        }).done(function(msg) {
            $(pos).html(msg);
            setTimeout(function(){
            location.reload();},3000);
        }); 
    }
    else {
           alert("Операция отменена!");
       }
    }
}
/*Delete Category Section END*/

/*Rename Category Section START*/
function rename_cat() {
    var send_data = $("#rcat-form").serialize();
    $.ajax({
        url:"?route=application/category/renameCategory",
        type:"POST",
        data:send_data,
        cache:false,
        beforeSend:function() {
            $(".info").html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
        }
    }).done(
    function(msg) {$(".info").html(msg);setTimeout(function(){location.reload();},3000);
    });
}

function rename_form(element,id) {
    var name = $(element).parent().children("span").text();
    var dlg = open_dialog();
    dlg.dialog({
        title:"Редактирование категории",
        buttons: {
            "Редактировать":function() {rename_cat();dlg.dialog("close");},
            "Отмена":function() {dlg.dialog("close");}
        }
    });
    dlg.load("?route=application/category/renameForm",function() {
                                                                    dlg.dialog("open");
                                                                    $("#rcat_name").val(name);
                                                                    $("#rcat_id").val(id);
                                                                    if($(element).parent().hasClass("def-cat")) {
                                                                        $("#rcat-def").prop("checked",true);
                                                                    }
                                                                    if($(element).parent().hasClass("childrencat")) {
                                                                        $("#rcat-def").prop("disabled", true);
                                                                    }
                                                                    $.post(
                                                                        "?route=application/category/getSortId",
                                                                        {
                                                                         id:id
                                                                        },
                                                                        function(data) {
                                                                            $("#sort").val(data);
                                                                        }
                                                                    );
                                                                });
}
/*Rename Category Section END*/
function open_dialog() {
    var dlg = $("#dialog").dialog({
        autoOpen:false,
        modal:true,
        hide:{
          effect:"clip",
          duration: 500
        },
        show:{
            effect:"clip",
            duration:500
        },
        close: function() {
            dlg.empty();
         dlg.dialog("destroy");   
        }
    });
    return dlg;
}

