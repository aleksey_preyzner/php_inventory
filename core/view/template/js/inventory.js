/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*Add Inv START*/
    function add_inv_dialog() {
        var dlg = open_dialog();
        dlg.dialog({
            title:"Добавить инвентарь",
            buttons: {
                "Добавить":function() {add_inv();dlg.dialog("close");},
                "Отмена":function() {dlg.dialog("close");}
            },
            minWidth:430,
            minHeight:350
        });
        dlg.load("?route=application/inventory/inventoryform",function() {dlg.dialog("open");});
    }
    function add_inv() {
        var inv_data = $('#inventory_form').serialize();
        $.ajax({
            type:'POST',
            url:'?route=application/inventory/add_inventory',
            cache:false,
            data:inv_data,
            beforeSend:function() {
            $('.info').html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
            },
            success:function(data) {
              $('.info').html(data);
              setTimeout(function() {location.reload();},3000);
            },
            error:function(xhr, str) {
                $('.info').html('<span class="error">Ошибка:'+ xhr.responseCode + '</span>');
            }
        });
    }
/*Add Inv END*/

/*Del Inv START*/
function inv_del() {
        var ids = $("input[type=checkbox]:checked").toArray();
        var idstr = "";
        for(var i=0;i < ids.length;i++) {
            idstr += ids[i].value + "_";
        }
        idstr = idstr.slice(0,-1);
        var accept = confirm("Вы действительно хотите удалить инвентарь?");
        if(accept) {
            $.ajax({
            url:'?route=application/inventory/delete_inventory',
            type:'POST',
            cache:false,
            data:"data=" + idstr,
            beforeSend:function() {
            $(".info").html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
            },
            success: function(data) {
               $(".info").html( data );
               setTimeout(function() {location.reload();},3000);
            },
            error:function(xhr,str) {
                $('.info').html('<span class="error">Ошибка:'+ xhr.responseCode + ' : ' + str + '</span>');
            }
            });
       }
       else {
           alert("Операция отменена!");
       }
}
/*Del Inv END*/

/*Edit Inv START*/
function info_inv_dialog(id) {
    var dlg = open_dialog();
    dlg.dialog({
        title:"Информация об инвентаре",
        buttons:{
            "Редактировать":function(){inv_change();dlg.dialog("close");},
            "Отмена":function() {dlg.dialog("close");}
        },
        minHeight:300,
        minWidth:600
    });
    dlg.load("?route=application/inventory/info&id="+id,function() {dlg.dialog("open");});
}
function cat_list() {
    $.ajax({
        url:"?route=application/category/listCategory",
        type:"POST",
        data:"ajax=1",
        dataType:"json",
        cache: false,
        success:function(data) {
            var catlist = data;
            var cat_options = '';
            $('#info_cat_id').empty();
            $('#info_cat_id').prop('disabled', false);
            
            for(var i = 0; i < catlist.length; i++) {
              $('#info_cat_id').append("<option value='"+ catlist[i].cat_id + "'>" + catlist[i].cat_name + "</option>");  
            }
        }
        });
};
function inv_change() {
    var form_data = $('#info_inv').serialize();
    if($('#info_cat_id').is(':disabled') == true) {
        form_data = form_data + '&info_cat_id='+ $('#info_cat_id').val();
    }
    $.ajax({
       url:"?route=application/inventory/change_inventory",
       type:"POST",
       cache:false,
       data:form_data,
       beforeSend:function() {
            $(".info").html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
        }
       }).done(function(message) {
               $(".info").html( message );
               setTimeout(function(){
                  location.reload(); 
               },3000);
           });   
    };
/*Edit Inv END*/
function open_dialog() {
    var dlg = $("#dialog").dialog({
        autoOpen:false,
        modal:true,
        hide:{
          effect:"clip",
          duration: 500
        },
        show:{
            effect:"clip",
            duration:500
        },
        close: function() {
            dlg.empty();
         dlg.dialog("destroy");   
        }
    });
    return dlg;
}

