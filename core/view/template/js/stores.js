/*Add Store START*/
function add_store_dialog(){
    var dlg = open_dialog();
    dlg.dialog({
        title:"Добавление нового склада",
        buttons:{
            "Добавить":function() {add_store();dlg.dialog("close");},
            "Отмена":function() {dlg.dialog("close");}
        },
        minWidth:400
    });
    dlg.load("?route=application/stores/addForm",function(){dlg.dialog("open");});
}
function add_store() {
    var formdata = $('#store-data').serialize();
    
    $.ajax({
        url:"?route=application/stores/addStore",
        data:formdata,
        type:"POST",
        cache:false,
        beforeSend:function() {
            $(".info").empty();
            $(".info").html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
        },
        success:function(data) {
            $(".info").children("img").remove();
            $(".info").html(data);
            setTimeout(function(){ location.reload();},3000);
        },
        error:function(xhr, str) {
                $('.info').html('<span class="error">Ошибка:'+ xhr.responseCode + '</span>');
            }
    });
}
/*Add Store END*/

/*Add Inv to Store START*/
function add_inv_to_store_dlg(s_id,s_name) {
    var dlg = open_dialog();
    dlg.dialog({
        title:"Добавление на склад " + s_name,
        buttons: {
            "Добавить":function() {
                if(($("#astore_com").val() === null || $("#astore_com").val() === "") || $("#astore_com").val().length < 25) {
                 $("#dialog").effect("highlight");
                 $(".informer").text("Необходимо указать комментарий! Длина минимум 25 символов!");
                 $("#astore_com").css("border-color","red");
                }
                else {
                add_inv_to_store();dlg.dialog("close");
                }
            ;},
            "Отмена":function() {dlg.dialog("close");}
        }
    });
    dlg.load("?route=application/stores/AddInvToStoreForm",function() {dlg.dialog("open");$("#astore_id").val(s_id);});
}
function add_inv_to_store() {
    var store_data = $("#add_store_inv").serialize();
    $.ajax({
        url:"?route=application/stores/addInvToStore",
        data:store_data,
        type:"POST",
        cache:false,
        beforeSend:function() {
            $(".info").empty();
            $(".info").html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");
        },
        success:function(data) {
            $(".info").children("img").remove();
            $(".info").html(data);
            setTimeout(function(){ location.reload();},3000);
        },
        error:function(xhr, str) {
            $('.info').html('<span class="error">Ошибка:'+ xhr.responseCode + '</span>');
        }
    });
}
/*Add Inv to Store END*/
/*Move-Delete Inv To Store START*/
function rem_inv_from_store_dlg(s_id,i_id,i_name) {
    var dlg = open_dialog();
    dlg.dialog({
        minWidth:400,
        minHeight:300,
        title:"Списание/удаление инвентаря " + i_name,
        buttons: {
            "Применить":function() {
                            rem_inv_from_store();
                            dlg.dialog("close");
                        },
            "Отмена":function() {dlg.dialog("close");}
        }
    });
    dlg.load("?route=application/stores/RemInvFromStoreForm",function() {
        $("#inv_id").val(i_id);
        $("#s_id").val(s_id);
        $.ajax({
         url:"?route=application/stores/getCurInvOnStore",
         type:"POST",
         data:"sid=" + s_id + "&iid=" + i_id,
         cache:false,
         success: function(data) {
             $("#max-val").val(data);
             $("#spinner").spinner("option","max",data);
             $("#spinner").spinner("option","min",0);
         }
        });
        $.ajax({
            url:"?route=application/stores/getOtherStores",
            type:"post",
            data:"sid=" + $("#s_id").val(),
            dataType:'json',
            success: function(data) {
                for(var i = 0; i < data.length; i++ ) {
                    $("#stores-lst").append("<option value='" + data[i].store_id + "'>" + data[i].store_name + "</option>");
                }
            }
        });
        dlg.dialog("open");});
}

function rem_inv_from_store() {
    var data = $("#rem_form").serialize();
    //alert(data);
    $.ajax({
        url:"?route=application/stores/RemoveInvFromStore",
        type:"POST",
        data: data,
        beforeSend:function() {
            $(".info").empty();
            $(".info").html("<img src='./core/view/template/image/ajax-loader.gif' border='0' style='padding-left:20px'/>");  
        },
        success:function(data) {
            $(".info").children("img").remove();
            $(".info").html(data);
            setTimeout(function(){ location.reload();},3000);
        },
        error:function() {}
    });
}
/*Move-Delete Inv To Store END*/

function open_dialog() {
    var dlg = $("#dialog").dialog({
        autoOpen:false,
        modal:true,
        hide:{
          effect:"clip",
          duration: 500
        },
        show:{
            effect:"clip",
            duration:500
        },
        close: function() {
            dlg.empty();
         dlg.dialog("destroy");   
        }
    });
    return dlg;
}