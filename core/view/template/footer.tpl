</div>
<div id="footer">
    <div id="copyright">
        By <a href="mailto:alexey.preyzner@gmail.com" title="send e-mail">Alexey Preyzner</a> &copy; 2013 - <?php echo date('Y') ?> |             
        <?php
        if(defined('SYSTEM_VERSION')) {
        echo "\"Inventory\" version ".SYSTEM_VERSION;
        }
        ?>
        </div>
</div>
</div>
</body>
</html>
