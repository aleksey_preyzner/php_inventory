<div class="block-title">
    <?php echo $title ?>
</div>
<div class="block-content">
    <div id="options">
        &nbsp; Показывать:<select id="limit" onChange="setLimit();">
            <option value="15">15</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select>
        </div>
<?php
if(isset($stores_logs)) { ?>
<center>
    <?php echo $pagination; ?>
    <table class="log" cellspacing="0" cellpadding="0">
 <tr class="log-title">
        <td>
            День/время
        </td>
         <td>
            Действие
        </td>
        <td>
            Склад
        </td>
        <td>
            Инвентарь
        </td>
        <td>
            Количество
        </td>
        <td width="350px">
            Комментарий
        </td>
    </tr>  
<?php foreach($stores_logs as $store_log) { ?>
    <tr>
        <td>
            <?php echo $store_log['act_date']; ?>
        </td>
        <td>
            <?php echo $store_log['act_type']; ?>
        </td>
        <td>
            <?php echo $store_log['store_name']; ?>
        </td>
        <td>
            <?php echo $store_log['inv_name']; ?><div class="infobar"><a class="info-link" onClick="javascript:info_inv_dialog(<?php echo $store_log['inv_id']; ?>);"></a></div>
        </td>
        <td>
            <?php echo $store_log['quantity']; ?>
        </td>
        <td>
            <?php echo $store_log['act_comment']; ?>
        </td>
    </tr>
    <?php } ?>
 </table><?php echo $pagination; ?></center>
<script>
     $("tr:even").addClass("even");
</script>
<? } else {
echo "Нет событий.";
} ?>   
    </div>