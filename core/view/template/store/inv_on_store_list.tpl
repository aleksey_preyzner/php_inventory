<?php
echo $header;
?>
<div id="left-column">
 <?php
 if(isset($menu)) {
 echo $menu;
 }
 ?>
</div>
<div id="center-column">
    <div class="block-title">
    <?php echo $title ?> (Всего на складе: <?php echo $total;?>)
</div>
    <div class="block-content">
        <div id="options">
            <span class="button" onClick="javascript:add_inv_to_store_dlg(<?php echo $store_id; ?>,'<?php echo $store_name; ?>');">Добавить инвентарь</span>
            <span class="info"></span>
        </div>
        <?php if(isset($inv_list)) { ?>
        <center><table class="log" cellspacing="0" cellpadding="0">
         <?php foreach($inv_list as $inv) { ?>
         <tr><td colspan="2" style="background-color: #ccc"><?php echo "<b>".$inv['cat_name']."</b>"; ?>
                 </td>     
         </tr>
                <?php foreach($inv['inv_list'] as $i) { ?>
                <tr>
                    <td style="padding-left: 15px;"><a href="javascript:void(0);" onClick="javascript:info_inv_dialog(<?php echo $i['inv_id']; ?>);"><?php echo $i['inv_name']; ?></a></td>
                    <td><a href="javascript:void(0);" onClick="javascript:rem_inv_from_store_dlg(<?php echo $store_id; ?>,<?php echo $i['inv_id']; ?>, '<?php echo $i['inv_name']; ?>');"><?php echo $i['inv_quantity']; ?></a></td>
                </tr>    
                <?php } ?>
         <?php } ?>
        </table><?php echo $pagination; ?></center>   
<script>
     $("tr:even").addClass("even");
</script>
<?php } else { echo "На складе нет инвентаря на хранении";}?>        
    </div>
</div>
<?php
echo $footer;
?>