<form id="add_store_inv" name="add_store_inv">
    <label for="add_inv">Инвентарь:</label>
    <span class="ui-spinner ui-widget ui-widget-content ui-corner-all">
        <select name="ainv_id" id="ainv_id" class="ui-spinner-input" style="width:100%">
    <?php if(isset($inv_list)) { ?>
    <?php foreach($inv_list as $inv) { ?>
    <option value="<?php echo $inv['inv_id'];?>"><?php echo $inv['inv_name']?></option>
    <?php }} else { ?>
    <option value='null'>Нет инвентаря!</option>
    <?php }?>
    </select>
    </span>
    <p>
    <label for="spinner">Количество:</label>
    <input id="spinner" name="aval"></input>
    </p>
    <input type="hidden" id="astore_id" name="astore_id"></input>
    <label>Комментарий:</label>
    <textarea name="astore_com" id="astore_com" rows="3" cols="30"></textarea>
    <span style="color:red" class="informer"></span>
</form>
<script>
    $(function() {
        $("#spinner").spinner();
    });
</script>