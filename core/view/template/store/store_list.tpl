<?php
echo $header;
?>
<div id="left-column">
 <?php
 if(isset($menu)) {
 echo $menu;
 }
 ?>
</div>
<div id="center-column">
<div class="block-title">
    <?php echo $title ?>
</div>
<div class="block-content">
    <div id="options"><span class='button' onClick="javascript:add_store_dialog();">Добавить новый склад</span>&nbsp;<span class="button" onClick="window.open('?route=application/reports')">Отчет по складам</span><span class="info"></span></div>
    <?php if(isset($stores)) {
        foreach($stores as $store) { ?>
        <div class="block-title"><strong><?php echo $store['store_name']; ?></strong><div style="float:right;"><input type="checkbox" value="<?php echo $store['store_id']; ?>"></div></div>
 <div class='block-content' style="padding:15px;">
        <div class="store-menu-block">
            <a class="store-menu" onClick="javascript:add_inv_to_store_dlg(<?php echo $store['store_id']; ?>,'<?php echo $store['store_name'] ?>');"><img src='http://<?php echo DIR_IMG; ?>store-add.png' title='Добавить инвентарь на склад'></a>
            <a class="store-menu" href="?route=application/stores/view_store_inv&store_id=<?php echo $store['store_id']; ?>"><img src='http://<?php echo DIR_IMG; ?>store-list.png' title='Посмотреть список инвентаря'></a>
            <a class="store-menu"><img src='http://<?php echo DIR_IMG; ?>store-report.png' title='отчет Excel'></a>
        </div>
     <strong>Описание:</strong><?php echo $store['store_desc']; ?><br />
 </div>
 <br/><br/>
<?php }
    }
    else {
    echo "Список складов пуст!";
    } ?>
</div>
</div>
<?php
echo $footer;
?>