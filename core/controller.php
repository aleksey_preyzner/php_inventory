<?php
defined('_INV') or die('Restricted access');

abstract class Controller {
    protected $registry;
    protected $data = array();
    protected $children = array();
    protected $template = '';
    protected $output;
            
        public function __construct($registry) {
                $this->registry = $registry;
        }
    	public function __get($key) {
		return $this->registry->get($key);
	}
	
	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
        
        protected function getChild($child,$args=array()) {
                $action = new Action($child,$args);
                if (file_exists($action->getFile())) {
			require_once($action->getFile());

			$class = $action->getClass();

			$controller = new $class($this->registry);

			$controller->{$action->getMethod()}($action->getArgs());
			
			return $controller->output;
                }
                        else {
            echo "Ошибка: Не могу найти файл шаблона ". $controller->template ."!";
        }
    }
    protected function render() {
        foreach($this->children as $child) {
            $this->data[basename($child)] = $this->getChild($child);
        }
        if(file_exists(DIR_THEME.$this->template)) {
            extract($this->data);
            ob_start();
            require DIR_THEME.$this->template;
            
            $this->output = ob_get_contents();
            
            ob_clean();

            return $this->output;
        }
        else {
            echo "<b class='err'>Ошибка: Не могу найти файл шаблона ".$this->template."!</b>";
        }
    }
}
