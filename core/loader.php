<?php
defined('_INV') or die('Restricted access');

final class Loader {
    protected $registry;
                
    function __construct($registry) {
        $this->registry = $registry;    
    }
    public function __get($key) {
		return $this->registry->get($key);
    }

    public function __set($key, $value) {
		$this->registry->set($key, $value);
    }
    public function model($model) {
        $file = DIR_CORE. "model/". $model .".php";
        $class = explode("/",$model);
        $class="Model_". end($class);
        if(file_exists($file)) {
            include_once $file;
            $this->registry->set('model_'.str_replace("/","_", $model),new $class($this->registry));
        }
        else {
            echo "Ошибка: Не могу загрузить модель ".$model."!";
        }
    }
    public function library($library) {
        $file = DIR_LIB.$library.".php" ;
        if(file_exists($file)) {
            include_once($file);
        }
        else {
            echo "Ошибка! Не могу загрузить библиотеку ".$library;
        }
    }
}


