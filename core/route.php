<?php
defined('_INV') or die('Restricted access');

final class Route {
    private $registry;

    public function __construct($registry) {
        $this->registry = $registry;
    }

    public function Start() {
        $controller_type = "default";
        $controller_name = "home";
        $action_name="index";
        $request = new Request();
        if(isset($request->get['route'])) {
            $route = $request->get['route'];
            $route_array = explode("/",$route);
        
            if(!empty($route_array[0])) {
              $controller_type = $route_array[0];
            }
            if(!empty($route_array[1])) {
              $controller_name = $route_array[1];
            }
            if(!empty($route_array[2])) {
                $action_name = $route_array[2];
            }
        }
        
        if(file_exists(DIR_CORE."controller/".$controller_type."/".$controller_name.".php")) {
            include DIR_CORE."controller/".$controller_type."/".$controller_name.".php";
        } else {
            $this->responce->redirect("/");
        }
        
        $controller_name = "Controller_".$controller_name;
        
        $controller=new $controller_name($this->registry);
        $controller->$action_name();
        
    } 
}
