<?php
defined('_INV') or die('Restricted access');

final class Document {
    protected $title;
    protected $scripts = array();

    function GetTitle() {
        return $this->title;        
    }
    function SetTitle($title) {
        $this->title = $title;        
    }
    public function addScript($script) {
	$this->scripts[md5($script)] = $script;			
    }
	
    public function getScripts() {
	return $this->scripts;
    }
    public function addStyle($href, $rel = 'stylesheet', $media = 'screen') {
		$this->styles[md5($href)] = array(
			'href'  => DIR_CSS.$href,
			'rel'   => $rel,
			'media' => $media
		);
	}

	public function getStyles() {
		return $this->styles;
	}
}
