<?php
defined('_INV') or die('Restricted access');

class Pagination {
    public $total = 0;
    public $page = 1;
    public $view_pages = 3;
    public $limit = 15;
    public $txt_last = "&raquo;";
    public $txt_first = "&laquo;";
    public $txt_next = "&#8250;";
    public $txt_prev = "&#8249;";
    public $txt_request = "";
    
    public function render() {
        if($this->page<1) {
            $this->page=1;
        }
        
        if(!(int)$this->limit) {
            $this->limit = 15;
        }
        $total_pages = ceil($this->total/$this->limit);
        $output = '<div class="pagination">';
        
        $start = $this->page - $this->view_pages;
        if($start < 1) {
            $start = 1;
        }
        $end = $this->page + $this->view_pages;
        if($end > $total_pages) {
            $end = $total_pages;
        }
        
        
        
        if($this->page > 1) {
          $output .= '<a href="'.$this->txt_request.'page=1">'.$this->txt_first.'</a><a href="'.$this->txt_request.'&page='.($this->page - 1).'">'.$this->txt_prev.'</a>';
          
        }
        if($start > 1) {
            $output .="...";
        }
        for ($i=$start;$i<=$end;$i++) {
            if($i == $this->page) {
                $output .= "<strong>".$i."</strong>";
            }
            else {
                $output .= '<a href="'.$this->txt_request.'&page='.$i.'">'.$i.'</a>';
            }
        }
        if($end<$total_pages) {
            $output .= "...";
        }
        if($this->page < $total_pages) {
            $output .= '<a href="'.$this->txt_request.'&page='.($this->page +1).'">'.$this->txt_next.'</a><a href="'.$this->txt_request.'&page='.$total_pages.'">'.$this->txt_last.'</a>';
        }
        if($total_pages <= 1) {
            $output = '';
        }
        else {
            $output .="</div>";
        }
            
      return $output;  
    }
   
}